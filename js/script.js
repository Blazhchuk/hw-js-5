"use strict"

/*

1.  Функцію можна створити за допомоги function, і в змінній. Викликати вункцію можна за допомоги імя функції та круглих дужок.

2.  Оператор return використовується для завершення функції і відбувається повернення значення до місця виклику.
    Використовується return безпосередьно в самій функції, також його не обовьязково вікористовувати в кінці функції.

3.  Параметри - це змінні які вказують у дужках під час створення функції.
    Аргументи - це конкретні значення, які передаються функції під час ії виклику.
    Використвоють коли створюється функція, яка потребує певних даних для операцій.

4.  При виклику другої функції вказати назву (в дужках) першої функції.

*/



// ex 1

function divide(a, b) {
    if (b === 0) {
        console.log('Error: Division by zero is not allowed');
        return null;
    }
    return a / b;
}

let numerator;
let denominator;

while (true) {
    numerator = +prompt('Enter numenator num:');
    if (!isNaN(numerator)) {
        break;
    }
    alert('Please enter a valid num for the numerator.');
}

while (true) {
    denominator = +prompt('Enter denominator num:')
    if (!isNaN(denominator) && denominator !== 0) {
        break;
    }
    alert('Please enter a valid non-zero num for the denominator');
}

const result = divide(numerator, denominator);

if (result !== null) {
    console.log(`Fraction ${numerator} on ${denominator} equal ${result}.`);
}
else {
    console.log('Error in calculating the share.');
}


// ex 2

function validateNumberInput(promptMessage) {
    while (true) {
        const userInput = prompt(promptMessage);
        const parsedNumber = parseFloat(userInput);
        if (!isNaN(parsedNumber)) {
            return parsedNumber;
        }
        alert('Please enter a valid number.');
    }
}

function validateOperatorInput() {
    while (true) {
        const operator = prompt('Enter the operator (+, -, *, /):');
        if (['+', '-', '*', '/'].includes(operator)) {
            return operator;
        }
        alert('Invalid operator. Please use +, -, *, or /.');
    }
}

function performOperation(a, b, operator) {
    switch (operator) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            if (b !== 0) {
                return a / b;
            } else {
                alert('Error: Division by zero is not allowed.');
                return null;
            }
        default:
            alert('Error: Invalid operator. Please use +, -, *, or /.');
            return null;
    }
}

const firstNumber = validateNumberInput('Enter the first number:');
const secondNumber = validateNumberInput('Enter the second number:');
const operator = validateOperatorInput();

if (firstNumber !== null && secondNumber !== null) {
    const result = performOperation(firstNumber, secondNumber, operator);
    if (result !== null) {
        console.log(`Result of ${firstNumber} ${operator} ${secondNumber} is ${result}.`);
    } else {
        alert('Error in performing the operation.');
    }
}


// ex 3

const validatePositiveInteger = (input) => {
    const parsedNumber = parseInt(input, 10);
    if (isNaN(parsedNumber) || parsedNumber <= 0) {
        alert('Please enter a valid positive integer.');
        return null;
    }
    return parsedNumber;
};

const calculateFactorial = (n) => {
    let result = 1;
    for (let i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
};

let userInput;
let number;

while (true) {
    userInput = prompt('Enter a positive integer:');
    number = validatePositiveInteger(userInput);
    if (number !== null) {
        break;
    }
}

const factorial = calculateFactorial(number);
console.log(`Factorial of ${number} is ${factorial}.`);
